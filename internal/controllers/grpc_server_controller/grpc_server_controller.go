package grpcservercontroller

import (
	"context"
	"sync"
	"time"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/ferrysourcing/ferrybroker.git/internal/models"
	"gitlab.com/ferrysourcing/ferrybroker.git/internal/servicies/raftservice"
	"gitlab.com/ferrysourcing/ferrybroker.git/pkg/api/raft"
)

type GRPCNodeServerImpl struct {
	raftService *raftservice.RaftService
	raft.UnimplementedRaftNodeServer
	nodeID                                 string
	raftNodesGUIDSbyURL                    map[string]string
	grpcNodeClientByGUID                   sync.Map
	grpcConnectionsByURL                   sync.Map
	grpcVoteResponseStreamsByGUID          sync.Map
	grpcAppendEntriesResponseStreamsByGUID sync.Map
	nodeURLs                               []string
}

func NewGRPCNodeServer(nodesURLs []string) raft.RaftNodeServer {
	grpcServer := &GRPCNodeServerImpl{
		nodeID:              uuid.New().String(),
		raftNodesGUIDSbyURL: make(map[string]string),
		nodeURLs:            nodesURLs,
	}
	raftService := raftservice.NewRaftService(grpcServer.nodeID, len(grpcServer.nodeURLs), grpcServer.SendToAllAppendEntriesImpl, grpcServer.SendRequestVoteImpl)
	grpcServer.raftService = raftService
	go grpcServer.RunDiscovering()
	return grpcServer
}

func (ns *GRPCNodeServerImpl) SendToAllAppendEntriesImpl(map[string]*models.AppendEntriesRequest, chan *models.AppendEntriesResponse) {
}

func (ns *GRPCNodeServerImpl) SendRequestVoteImpl(string, *models.RequestVoteRequest) {
}

func (ns *GRPCNodeServerImpl) WhoIsWho(context.Context, *raft.WhoIs) (*raft.DiagnosticData, error) {
	return &raft.DiagnosticData{
		NodeGUID:     ns.nodeID,
		NodesMapping: ns.raftNodesGUIDSbyURL,
		//TODO: Список состояний нод по топикам
		// Role: ns.raftService.Role.ToString(),
	}, nil
}

func (ns *GRPCNodeServerImpl) RunDiscovering() {
	log.Infoln("RunDiscovering...")
	for {
		ns.dicoverNodes()
		time.Sleep(1 * time.Second)
	}
}
