package main

import (
	"fmt"
	"strconv"

	"net"
	"os"

	log "github.com/sirupsen/logrus"
	grpcservercontroller "gitlab.com/ferrysourcing/ferrybroker.git/internal/controllers/grpc_server_controller"
	"gitlab.com/ferrysourcing/ferrybroker.git/pkg/api/raft"
	"google.golang.org/grpc"
)

func main() {
	log.SetLevel(log.TraceLevel)
	GRPC_PORT := os.Getenv("GRPC_PORT")
	if GRPC_PORT == "" {
		GRPC_PORT = "50051"
	}
	NODES_NUMBER_STR := os.Getenv("NODES_NUMBER")
	NODES_NUMBER, err := strconv.Atoi(NODES_NUMBER_STR)
	if NODES_NUMBER <= 0 || err != nil {
		log.Fatalln("nodes number must be greater than one ", err)
	}
	var nodesURLs = make([]string, NODES_NUMBER)
	for nodeNumber := 0; nodeNumber < NODES_NUMBER; nodeNumber++ {
		nodesURLs[nodeNumber] = os.Getenv(fmt.Sprintf("NODE_HOST_%d", nodeNumber))
	}

	log.Infoln(nodesURLs)

	listener, err := net.Listen("tcp", fmt.Sprintf("0.0.0.0:%s", GRPC_PORT))
	if err != nil {
		log.Fatalln(err)
	}
	var opts []grpc.ServerOption
	grpcServer := grpc.NewServer(opts...)
	raft.RegisterRaftNodeServer(grpcServer, grpcservercontroller.NewGRPCNodeServer(nodesURLs))
	log.Infof("starting at %s", listener.Addr().String())
	err = grpcServer.Serve(listener)
	if err != nil {
		log.Fatalln("gRPC serve error ", err)
	}
}
